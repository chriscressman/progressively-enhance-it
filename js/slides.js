(function () {
    'use strict';

    var MODE = 'slides', // use 'notes' for notes view

        $slides = $('.slide'),

        updateSlides = function ($currentSlide) {
            $slides.removeClass('current-slide');
            $currentSlide.addClass('current-slide');
        },

        detectCurrentSlide = function () {
            return $slides.filter(function () {
                var rect = this.getBoundingClientRect();
                return rect.top > 0;
            }).first();
        },

        detectThenUpdate = _.compose(updateSlides, detectCurrentSlide),

        initSlides = function () {
            $('body').addClass('slides');

            $slides.on('click', function () {
                updateSlides($(this));
            });

            $(window).on('scroll', _.debounce(detectThenUpdate, 250));

            detectThenUpdate();
        };

    if (MODE === 'slides') {
        initSlides();
    }
}());
