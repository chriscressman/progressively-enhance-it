Progressively Enhance It! with jQuery, Underscore & Modernizr
=============================================================

To view presentation slides, open _slides.html_ in a browser and scroll.

To view the slides as notes, edit the value of `MODE` in _js/slides.js_.

© 2013 Chris Cressman, WebLinc, LLC.

Text licensed under Creative Commons Attribution 3.0 Unported.
